# Outbound Default Security Group
resource "aws_default_security_group" "default-public-sec-group" {
  depends_on = [aws_vpc.eks-vpc]
  vpc_id = aws_vpc.eks-vpc.id
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  tags = {
    Name = "allow-all-outbound"
    Environment = terraform.workspace
  }
}

# allow all traffic -- control-plane <-> worker node
resource "aws_security_group" "allow-all-self" {
  depends_on = [aws_vpc.eks-vpc]
  vpc_id = aws_vpc.eks-vpc.id
  name = "allow-all-self"
  description = "allow-all-self"
  tags = {
    Name = "allow-all-inbound"
    Environment = terraform.workspace
  }
}
 
# allow all traffic -- control-plane <-> worker node
resource "aws_security_group_rule" "allow-all-self-rules" {
  depends_on = [aws_security_group.allow-all-self]
  from_port = 0
  protocol = "-1"
  security_group_id = aws_security_group.allow-all-self.id
  source_security_group_id = aws_security_group.allow-all-self.id
  to_port = 0
  type = "ingress"
}
