# Launch AWS EKS Cluster

A simple terraform plan to launch AWS EKS cluster

## Requirement
* AWS Key

## TODO
* ~~Create proper security group~~
* Identify how to loop inside a variable

## Post-Deployment
### Configure kubectl
aws eks --region ap-southeast-1 update-kubeconfig --name my-eks

kubectl get nodes

