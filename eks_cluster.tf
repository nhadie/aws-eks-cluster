# EKS Cluster
resource "aws_eks_cluster" "my-eks" {
  depends_on = [aws_iam_role.eksClusterRole,aws_iam_role_policy_attachment.eksClusterRoleAttachPolicy]
  name = "my-eks"
  role_arn = aws_iam_role.eksClusterRole.arn
  vpc_config {
    subnet_ids = [
      # Need to figure out how to loop inside a variable
      aws_subnet.eks-public-subnet["az1"].id,
      aws_subnet.eks-public-subnet["az2"].id,
      aws_subnet.eks-public-subnet["az3"].id,
      aws_subnet.eks-private-subnet["az1"].id,
      aws_subnet.eks-private-subnet["az2"].id,
      aws_subnet.eks-private-subnet["az3"].id
    ]
    security_group_ids = [
      aws_security_group.allow-all-self.id
    ]
    endpoint_private_access = true
    endpoint_public_access = true
###    public_access_cidrs = ["add your subnets here"]
  }
}

# EKS Node Group
resource "aws_eks_node_group" "my-eks-worker-nodes" {
  depends_on = [aws_eks_cluster.my-eks,aws_iam_role.eksNodeRole]
  cluster_name = aws_eks_cluster.my-eks.name
  node_group_name = "my-eks-worker-nodes"
  node_role_arn = aws_iam_role.eksNodeRole.arn
  instance_types = ["t2.micro"]
  # Need to figure out how to loop inside a variable
  subnet_ids = [
    # Need to figure out how to loop inside a variable
    aws_subnet.eks-private-subnet["az1"].id,
    aws_subnet.eks-private-subnet["az2"].id,
    aws_subnet.eks-private-subnet["az3"].id
  ]
  scaling_config {
    desired_size = 3
    max_size = 3
    min_size = 3
  }
}
